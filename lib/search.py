# -*- coding: utf-8 -*-

from lib.color import *


def getMatricula(name):
    dict = {}

    with open('./docs/database.csv', 'r') as file:
        for index, line in enumerate(file):
            if name in line.lower():
                dict[line.split(',')[1][:-1]] = line.split(',')[0]

    file.close()

    return filtro(dict)


def filtro(dict):
    if len(dict) < 1:
        print R + '\n [!]' + O + " Aluno" + R + " inválido" + O + " ou" + R + " inexistente\n" + W
        exit()
    elif len(dict) == 1:
        return dict.values()[0]
    else:
        return choose(dict)


def choose(dict):
    i = 0

    for key in dict.keys():
        print C + " [" + G + str(i) + C + "] " + W + key
        i += 1

    choice = raw_input("\n" + G + " [+]" + R + " Escolha a opção desejada: " + W)
    print ""
    return dict.values()[int(choice)]

