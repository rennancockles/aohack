# -*- coding: utf-8 -*-

import urllib


class Notas(object):

    def __init__(self, matricula):

        self.page = ""
        self.notas = ""

        self.connect(matricula)
        self.getDados()

    def connect(self, matricula):
        url = "https://www.alunoonline.uerj.br/notasdoperiododoaluno/notasparciais.php"
        data = {"matricula": matricula}

        param = urllib.urlencode(data)
        print urllib.urlopen(url, param).read()
        self.page = urllib.urlopen(url, param).read()

    def getDados(self):
        start = self.page.find('inscrito em nenhuma discip')
        stop = self.page.find('</font>', start)
        print start
        # print self.page
        if start != -1:

            self.notas = "Você não está "+self.page[start:stop]
