# -*- coding: utf-8 -*-

import urllib
from lib.color import *


class DadosPessoais(object):

    def __init__(self, matricula):

        self.page = ""
        self.nasc = ""
        self.sexo = ""
        self.est_civil = ""
        self.naturalidade = ""
        self.nacionalidade = ""
        self.nomePai = ""
        self.nomeMae = ""
        self.cpf = ""
        self.identidade = ""
        self.orgaoID = ""

        self.connect(matricula)
        self.getDados()

    def connect(self, matricula):
        url = "https://www.alunoonline.uerj.br/recadastramento_dados_pessoais/recadastramento_dados_pessoais.php"
        data = {"matricula": matricula}

        param = urllib.urlencode(data)

        self.page = urllib.urlopen(url, param).read()

    def filtro(self, param):
        if param.find("Informado") == -1:
            return param
        else:
            return ""

    def getDados(self):
        self.getNasc()
        self.getSexo()
        self.getEstCivil()
        self.getNaturalidade()
        self.getNacionalidade()
        self.getNomeMae()
        self.getNomePai()
        self.getCpf()
        self.getIdentidade()

    def getNasc(self):
        init = self.page.find("Data de Nasc.:</b>")
        stop = self.page.find("</font>", init)
        if init != -1:
            init += 41
            self.nasc = self.page[init:stop]

    def getSexo(self):
        init = self.page.find("Sexo:</b>")
        stop = self.page.find("</font>", init)
        if init != -1:
            init += 32
            self.sexo = self.page[init:stop]

    def getEstCivil(self):
        init = self.page.find("Est. Civil:</b>")
        stop = self.page.find("</font>", init)
        if init != -1:
            init += 38
            self.est_civil = self.filtro(self.page[init:stop])

    def getNaturalidade(self):
        init = self.page.find("Naturalidade:</b>")
        stop = self.page.find("</font>", init)
        if init != -1:
            init += 40
            self.naturalidade = self.page[init:stop]

    def getNacionalidade(self):
        init = self.page.find("Nacionalidade:</b>")
        stop = self.page.find("</font>", init)
        if init != -1:
            init += 41
            self.nacionalidade = self.page[init:stop]

    def getNomePai(self):
        init = self.page.find("Nome do Pai:</b>")
        stop = self.page.find("</font>", init)
        if init != -1:
            init += 39
            self.nomePai = self.page[init:stop]

    def getNomeMae(self):
        init = self.page.find("Nome da M")
        stop = self.page.find("</font>", init)
        if init != -1:
            init += 39
            self.nomeMae = self.page[init:stop]

    def getCpf(self):
        init = self.page.find("CPF:")
        stop = self.page.find("</font>", init)
        if init != -1:
            init += 31
            self.cpf = self.page[init:stop]

    def getIdentidade(self):
        init = self.page.find("IDENTIDADE")
        init = self.page.find("mero:", init)
        stop = self.page.find("</font>", init)
        if init != -1:
            init += 32
            self.identidade = self.page[init:stop]

        init = self.page.find("rg", init)
        stop = self.page.find("</font>", init)
        if init != -1:
            init += 33
            self.orgaoID = self.page[init:stop]

        init = self.page.find("UF:", init)
        stop = self.page.find("</font>", init)
        if init != -1:
            init += 30
            self.orgaoID = self.orgaoID + " - " + self.page[init:stop]

    def __str__(self):
        pattern    = D + "-----------------" \
                     "\n" + K + "Data de Nascimento: " + V + "{0}" \
                     "\n" + K + "Sexo: " + V + "{1}" \
                     "\n" + K + "Estado Civil: " + V + "{2}" \
                     "\n" + K + "Naturalidade: " + V + "{3}" \
                     "\n" + K + "Nacionalidade: " + V + "{4}" \
                     "\n" + K + "Nome do Pai: " + V + "{5}" \
                     "\n" + K + "Nome da Mãe: " + V + "{6}" \
                     "\n" + K + "CPF: " + V + "{7}" \
                     "\n" + K + "Identidade: " + V + "{8}" \
                     "\n" + K + "Órgão: " + V + "{9}" + W

        parameters = [self.nasc, self.sexo, self.est_civil, self.naturalidade, self.nacionalidade, self.nomePai, self.nomeMae, self.cpf, self.identidade, self.orgaoID]

        return pattern.format(*parameters)
