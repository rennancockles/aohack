# aohack
Programa para obter os dados de um aluno da UERJ através do número da matrícula ou do nome do aluno (apenas para alunos do IPRJ)

## Exemplos

```shell
./aohack.py 201010059121 -c
./aohack.py 'bruno silva' -r
```

## Help

```shell
./aohack.py -h
```
