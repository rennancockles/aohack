# -*- coding: utf-8 -*-

from lib.color import *


class Creditos(object):

    def __init__(self, page):
        self.obrig_obtidos = 0
        self.obrig_restante = 0
        self.elet_obtidos = 0
        self.elet_restante = 0

        self.getObrigatorias(page)
        self.getEletivas(page)

    def getObrigatorias(self, page):
        obrig = page.find("em Disciplinas Obrigat")
        obtidos = page.find("width:75", obrig)
        obtidos_stop = page.find("</div>", obtidos)
        restante = page.find("width:75", obtidos_stop)
        restante_stop = page.find("</div>", restante)
        if obtidos != -1 and restante != -1:
            obtidos += 43
            restante += 43
            self.obrig_obtidos = page[obtidos:obtidos_stop]
            self.obrig_restante = page[restante:restante_stop]

    def getEletivas(self, page):
        elet = page.find("em Disciplinas Eletivas")
        obtidos = page.find("width:75", elet)
        obtidos_stop = page.find("</div>", obtidos)
        restante = page.find("width:75", obtidos_stop)
        restante_stop = page.find("</div>", restante)
        if obtidos != -1 and restante != -1:
            obtidos += 43
            restante += 43
            self.elet_obtidos = page[obtidos:obtidos_stop]
            self.elet_restante = page[restante:restante_stop]

    def __str__(self):
        pattern    = "\n" + T + "Cŕeditos Obrigatórios:" \
                     "\n     " + K + "Obtidos: " + V + "{0}" \
                     "\n     " + K + "Restante: " + V + "{1}" \
                     "\n" + T + "Cŕeditos Eletivas:" \
                     "\n     " + K + "Obtidos: " + V + "{2}" \
                     "\n     " + K + "Restante: " + V + "{3}" + W

        parameters = [self.obrig_obtidos, self.obrig_restante, self.elet_obtidos, self.elet_restante]

        return pattern.format(*parameters)