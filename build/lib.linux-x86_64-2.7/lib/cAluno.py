# -*- coding: utf-8 -*-

import urllib
from lib.color import *
from lib.cDadosPessoais import DadosPessoais
from lib.cDadosContato import DadosContato
from lib.cSintese import SinteseFormacao
from lib.cRequisitosACursar import Cursar


class Aluno(object):

    def __init__(self, matricula, cursar):
        self.page = ""
        self.code = 0
        self.matricula = matricula
        self.nome = ""
        self.dadosPessoais = DadosPessoais(matricula)
        self.dadosContato = DadosContato(matricula)
        self.sintese = SinteseFormacao(matricula)

        if cursar:
            self.cursar = Cursar(matricula)
        else:
            self.cursar = ""

    def connect(self, matricula):
        url = "https://www.alunoonline.uerj.br/requisicaoaluno/requisicao.php?"
        data = {"matricula": matricula, "requisicao": "SinteseFormacao"}

        param = urllib.urlencode(data)
        page = urllib.urlopen(url, param)
        self.code = page.getcode()
        self.page = page.read()

    def getDados(self):
        self.connect(self.matricula)
        self.getNome()

    def getNome(self):
        start = self.page.find(self.matricula+" - ")
        stop = self.page.find("<", start)
        if start != -1 and stop != -1:
            start += 15
            self.nome = self.page[start:stop]

    def printAluno(self):
        print ""
        print "Nome: {}".format(self.nome)
        print "Matrícula: {}".format(self.matricula)
        print ""
        self.dadosPessoais.printPessoais()
        print ""
        self.dadosContato.printContato()
        print ""
        self.sintese.printSintese()
        print ""

    def __str__(self):
        pattern    = "\n" + K + "Nome: " + V + "{0}" \
                     "\n" + K + "Matrícula: " + V + "{1}" + W + \
                     "\n{2}" \
                     "\n{3}" \
                     "\n{4}\n" \
                     "\n{5}\n"

        parameters = [self.nome, self.matricula, self.dadosPessoais, self.dadosContato, self.sintese, self.cursar]

        return pattern.format(*parameters)
