# -*- coding: utf-8 -*-

import urllib
from lib.color import *


class Disciplina(object):
    def __init__(self, page, init):
        self.init = init
        self.cod = ""
        self.nome = ""
        self.periodo = ""
        self.conflito = ""

        self.getDados(page)

    def getDados(self, page):
        self.getCode(page)
        self.getNome(page)
        self.getPeriodo(page)
        self.getConflito(page)

    def getCode(self, page):
        cod_start = page.find("width=", self.init)
        cod_stop = page.find('<b>', cod_start)

        if cod_start != -1:
            cod_start += 36
            cod = page[cod_start:cod_stop]
            cod_start = cod_stop + 3
            cod_stop = page.find('</b>', cod_start)
            cod += page[cod_start:cod_stop]

            self.cod = cod
            self.init = cod_stop

    def getNome(self, page):
        nome_start = page.find(";", self.init)
        nome_stop = page.find('\n', nome_start)

        if nome_start != -1:
            nome_start += 1
            nome = page[nome_start:nome_stop]

            self.nome = nome
            self.init = nome_stop

    def getPeriodo(self, page):
        init = page.find(">Consulte<", self.init)
        periodo_start = page.lower().find("<td>", init)
        periodo_stop = page.lower().find('</td>', periodo_start)

        if periodo_start != -1:
            periodo_start += 4
            periodo = page[periodo_start:periodo_stop]

            self.periodo = periodo
            self.init = periodo_stop

    def getConflito(self, page):
        conflito_start = page.lower().find("<td>", self.init)
        conflito_stop = page.lower().find('</td>', conflito_start)

        if conflito_start != -1:
            conflito_start += 4
            conflito = page[conflito_start:conflito_stop]

            if conflito.lower().startswith("n"):
                conflito = "Não"

            self.conflito = conflito
            self.init = conflito_stop

    def __str__(self):
        pattern    = D + "-----------------" \
                     "\n" + K + "Código: " + V + "{0}" \
                     "\n" + K + "Nome: " + V + "{1}" \
                     "\n" + K + "Período Sugerido: " + V + "{2}" \
                     "\n" + K + "Permite Conflito: " + V + "{3}" + W

        parameters = [self.cod, self.nome, self.periodo, self.conflito]

        return pattern.format(*parameters)


class Cursar(object):

    def __init__(self, matricula):

        self.page = ""
        self.disciplinas = []

        self.connect(matricula)
        self.getDisciplinas()

    def connect(self, matricula):
        url = "https://www.alunoonline.uerj.br/requisicaoaluno/requisicao.php?"
        data = {"matricula": matricula, "requisicao": "RequisitosACursar"}

        param = urllib.urlencode(data)
        page = urllib.urlopen(url, param)

        self.page = page.read()

    def getDisciplinas(self):
        init = 0
        while True:
            find = self.page.find("class='LINKNAOSUB'", init)
            if find == -1:
                break

            d = Disciplina(self.page, find)

            self.disciplinas.append(d)

            init = d.init

    def __str__(self):
        pattern = D + "-----------------" + \
                  T + "\nDisciplinas a cursar:" + \
                  "\n{}" + W

        parameters = [str(d) for d in self.disciplinas]
        s = "\n".join(parameters)

        return pattern.format(s)

