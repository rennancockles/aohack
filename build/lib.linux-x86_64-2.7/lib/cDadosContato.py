# -*- coding: utf-8 -*-

import urllib
from lib.color import *


class DadosContato(object):

    def __init__(self, matricula):

        self.page = ""
        self.telefone1 = ""
        self.telefone2 = ""
        self.email = ""
        self.endereco = ""
        self.bairro = ""
        self.cidade = ""
        self.uf = ""
        self.cep = ""

        self.connect(matricula)
        self.getDados()

    def connect(self, matricula):
        url = "https://www.alunoonline.uerj.br/recadastramento_dados_contato/recadastramento_dados_contato.php"
        data = {"matricula": matricula}

        param = urllib.urlencode(data)

        self.page = urllib.urlopen(url, param).read()

    def getDados(self):
        self.getTelefone1()
        self.getTelefone2()
        self.getEmail()
        self.getEndereco()
        self.getBairro()
        self.getCidade()
        self.getUF()
        self.getCEP()

    def getTelefone1(self):
        ddd_init = self.page.find('name="num_ddd_1_pag"')
        ddd_start = self.page.find("value=", ddd_init)
        ddd_stop = self.page.find('" size=', ddd_start)

        tel_init = self.page.find('name="num_tel_1_pag"')
        tel_start = self.page.find("value=", tel_init)
        tel_stop = self.page.find('" size=', tel_start)

        if ddd_start != -1 and tel_start != -1:
            ddd_start += 7
            tel_start += 7

            ddd = self.page[ddd_start:ddd_stop]
            tel = self.page[tel_start:tel_stop]

            self.telefone1 = ddd + tel

    def getTelefone2(self):
        ddd_init = self.page.find('name="num_ddd_2_pag"')
        ddd_start = self.page.find("value=", ddd_init)
        ddd_stop = self.page.find('" size=', ddd_start)

        tel_init = self.page.find('name="num_tel_2_pag"')
        tel_start = self.page.find("value=", tel_init)
        tel_stop = self.page.find('" size=', tel_start)

        if ddd_start != -1 and tel_start != -1:
            ddd_start += 7
            tel_start += 7

            ddd = self.page[ddd_start:ddd_stop]
            tel = self.page[tel_start:tel_stop]

            self.telefone2 = ddd + tel

    def getEmail(self):
        init = self.page.find('E-Mail:')
        start = self.page.find("value=", init)
        stop = self.page.find('" size=', start)

        if start != -1:
            start += 7

            self.email = self.page[start:stop]

    def getEndereco(self):
        init = self.page.find('<b>Endere')
        start = self.page.find("value=", init)
        stop = self.page.find('" size=', start)
        if start != -1:
            start += 7

            self.endereco = self.page[start:stop]

    def getBairro(self):
        init = self.page.find('Bairro:')
        end = self.page.find('CEP:')

        type = self.page.find("input type=", init, end)

        if type != -1:
            start = self.page.find("value=", init)
            stop = self.page.find('" size=', start)
            if start != -1 and start != -1:
                start += 7

                self.bairro = self.page[start:stop]
        else:
            start = self.page.find("selected>", init)
            stop = self.page.find('</option>', start)
            if start != -1:
                start += 9

                self.bairro = self.page[start:stop]

    def getCidade(self):
        init = self.page.find('<b>Munic')
        start = self.page.find("selected>", init)
        stop = self.page.find('</option>', start)
        if start != -1:
            start += 9

            self.cidade = self.page[start:stop]

    def getUF(self):
        init = self.page.find('<b>UF:')
        start = self.page.find("selected>", init)
        stop = self.page.find('</option>', start)
        if start != -1:
            start += 9

            self.uf = self.page[start:stop]

    def getCEP(self):
        init = self.page.find('CEP:')
        start = self.page.find("value=", init)
        stop = self.page.find('" size=', start)
        if start != -1:
            start += 7

            self.cep = self.page[start:stop]

    def __str__(self):
        pattern    = D + "-----------------" \
                     "\n" + K + "Telefone 1: " + V + "{0}" \
                     "\n" + K + "Telefone 2: " + V + "{1}" \
                     "\n" + K + "Email: " + V + "{2}" \
                     "\n" + K + "Endereço: " + V + "{3}" \
                     "\n" + K + "Bairro: " + V + "{4}" \
                     "\n" + K + "Cidade: " + V + "{5}" \
                     "\n" + K + "UF: " + V + "{6}" \
                     "\n" + K + "CEP: " + V + "{7}" + W

        parameters = [self.telefone1, self.telefone2, self.email, self.endereco, self.bairro, self.cidade, self.uf, self.cep]

        return pattern.format(*parameters)

