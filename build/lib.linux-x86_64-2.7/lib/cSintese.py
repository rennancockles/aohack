# -*- coding: utf-8 -*-

import urllib
from lib.cCreditos import Creditos
from lib.color import *


class SinteseFormacao(object):

    def __init__(self, matricula):

        self.page = ""
        self.titulacao = ""
        self.curso = ""
        self.cr = ""
        self.creditos = None
        self.periodos_utilizados = 0
        self.periodos_restantes = 0

        self.connect(matricula)
        self.getDados()

    def connect(self, matricula):
        url = "https://www.alunoonline.uerj.br/requisicaoaluno/requisicao.php?"
        data = {"matricula": matricula, "requisicao": "SinteseFormacao"}

        param = urllib.urlencode(data)

        self.page = urllib.urlopen(url+param).read()

    def getDados(self):
        self.getTitulacao()
        self.getCurso()
        self.getCR()
        self.getPeriodos()
        self.getCreditos()

    def getTitulacao(self):
        start = self.page.find("Titula")
        stop = self.page.find("</div>", start)
        if start != -1 and stop != -1:
            start += 15
            self.titulacao = self.page[start:stop]

    def getCurso(self):
        start = self.page.find("Curso:")
        stop = self.page.find("</div>", start)
        if start != -1 and stop != -1:
            start += 12
            self.curso = self.page[start:stop]

    def getCR(self):
        start = self.page.find("C.R. Acumulado:")
        stop = self.page.find("</div>", start)
        if start != -1 and stop != -1:
            start += 20
            self.cr = self.page[start:stop]

    def getPeriodos(self):
        periodos = self.page.find("odos Utilizados/Em Uso")
        utilizados = self.page.find("</b>", periodos)
        utilizados_stop = self.page.find("</div>", utilizados)
        restante = self.page.find("</b>", utilizados_stop)
        restante_stop = self.page.find("</div>", restante)
        if utilizados != -1 and restante != -1:
            utilizados += 5
            restante += 5
            self.periodos_utilizados = self.page[utilizados:utilizados_stop]
            self.periodos_restantes = self.page[restante:restante_stop]

    def getCreditos(self):
        self.creditos = Creditos(self.page)

    def __str__(self):
        pattern    = D + "-----------------" \
                     "\n" + K + "Titulação: " + V + "{0}" \
                     "\n" + K + "Curso: " + V + "{1}" \
                     "\n" + K + "CR: " + V + "{2}" \
                     "\n{3}" \
                     "\n\n" + K + "Períodos Utilizados: " + V + "{4}" \
                     "\n" + K + "Períodos Restantes: " + V + "{5}" + W

        parameters = [self.titulacao, self.curso, self.cr, self.creditos, self.periodos_utilizados, self.periodos_restantes]

        return pattern.format(*parameters)
