#!/usr/bin/python -B
# -*- coding: utf-8 -*-

from lib.cAluno import Aluno
from lib.color import *
from lib.search import getMatricula
import argparse
import time
import itertools
import threading
import sys


def Banner():

    print C+"   ___   __              "+ C +"   ____       ___          "
    print C+"  / _ | / /_ _____  ___  "+ C +"  / __ \___  / (_)__  ___  "
    print C+" / __ |/ / // / _ \/ _ \ "+ C +" / /_/ / _ \/ / / _ \/ -_) "
    print C+"/_/ |_/_/\_,_/_//_/\___/ "+ C +" \____/_//_/_/_/_//_/\__/  "

    print G + "\t\t   __ __         __   "
    print G + "\t\t  / // /__ _____/ /__ "
    print G + "\t\t / _  / _ `/ __/  '_/ "
    print G + "\t\t/_//_/\_,_/\__/_/\_\  " + W

    print ""


def load():
    for c in itertools.cycle(['|', '/', '-', '\\']):
        if done:
            break
        sys.stdout.write('\r' + G + " [+]" + W + ' Loading ' + R + c)
        sys.stdout.flush()
        time.sleep(0.1)
    sys.stdout.write(W + '\r              ')


parser = argparse.ArgumentParser(
    prog='aohack.py',
    usage='%(prog)s matricula\n'+
          '\t\b%(prog)s nome -r')

parser.add_argument("m", metavar="matricula", type=str, help="numero da matricula")
parser.add_argument("-r", action="store_true", help="busca por nome de aluno (apenas IPRJ)")
parser.add_argument("-c", action="store_true", help="exibe disciplinas a cursar")
done = False

Banner()
args = parser.parse_args()

if args.r:
    matricula = getMatricula(args.m)
else:
    matricula = args.m


threading.Thread(target=load).start()
aluno = Aluno(matricula, args.c)

time.sleep(2)
done = True

aluno.getDados()

if aluno.code == 500:
    print R + '\n [!]' + O + " Matrícula" + R + " inválida" + O + " ou" + R + " inexistente\n" + W
    exit()
else:
    print aluno
