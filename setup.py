#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

setup(name='aohack',
      version='1.0',
      description='Aluno Online UERJ Hack',
      long_description='Programa para obter os dados de um aluno da UERJ '+
                    'através do número da matrícula ou do nome do aluno '+
                    '(apenas para alunos do IPRJ)',
      author='Rennan Cockles',
      author_email='rcdev@hotmail.com.br',
      url='https://gitlab.com/rennancockles/aohack',
      download_url = 'https://gitlab.com/rennancockles/aohack.git',
      packages=['lib'],
      py_modules = ['aohack'],
      data_files=[('docs', ['docs/database.csv'])],
      license = 'MIT',
      install_requires=['setuptools'],
)
